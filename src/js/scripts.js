(function ($, window, document, undefined) {

    'use strict';

    $(function () {
        // start up the carousel
        $(".carousel").carousel({
            interval: 5000
        });

        // make the loading... text appear on paypal button
        $(".paypal").click(function() {
            $(this).css("display", "none");
            $(".paypal-loading").css("display", "block");
        });
    });

})(jQuery, window, document);
